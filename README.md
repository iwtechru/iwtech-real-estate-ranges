This plugin allows to attach date ranges and prices to different wordpress objects. Applicable for hotels, car rents, etc. Requires Jquery UI
Only one file, no data is thrown to header!
Use iwt_get_periods($pid) in your theme functions.php. $pid is not required argument, function takes current post id if no value is passed.