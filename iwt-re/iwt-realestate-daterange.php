<?php
/*
Plugin Name: iwtech attach date ranges
Plugin URI:  http://iwtech.ru
Description: This plugin allows to attach date ranges and prices to different wordpress objects. Applicable for hotels, car rents, etc. Requires jquery ui
Author: Nikita Menshutin
Version: 1.0b
Author URI:  http://iwtech.ru
*/

register_activation_hook(ABSPATH.PLUGINDIR.'/iwt-realestate-daterange/iwt-realestate-daterange.php','iwt_re_install');
add_action('admin_menu', 'iwt_re_menu');
add_action( 'add_meta_boxes', 'iwt_re_addbox' );
add_action('save_post', 'iwt_re_save');

function iwt_re_addbox(){
	add_meta_box('iwt-re', 'IWT sesons and prices', 'iwt_re_metabox', 'reobj', 'normal', 'default');
}
function iwt_get_periods($pid=0){
	// getting published periods.
	// !!!! Use this function for getting rows for your theme !
	global $wpdb;
	if ($pid==0) { // is post id is not passed to function, current post is taken
		global $post;
		$pid=$post->ID;
	}	
	$query='select postid, title, visible,
	min(date) as startdate,
	max(date) as enddate,
	price, status
	from `'.$wpdb->prefix.'iwt_re_periods` WHERE postid='.(int)$pid.'
	AND visible=1
	group by price
	order by startdate';	
	$periods = $wpdb->get_results($query);
	foreach ($periods as $per){
		$period[$per->title][]=$per;
	}
	return $period;
}
function iwt_re_metabox($post){
// iwt_re_metabox queries all rows, finds the same price values within definite periods and outputs each period as table row in html form
	global $wpdb;
	$query='select postid, title, visible,
	min(date) as startdate,
	max(date) as enddate,
	price, status
	from `'.$wpdb->prefix.'iwt_re_periods` WHERE postid='.(int)$post->ID.'
	group by price
	order by startdate';
	$periods = $wpdb->get_results($query);
	wp_nonce_field( plugin_basename(__FILE__), 'iwt_re_plugin' );
	?>
	<table style="width:90%; margin: auto;">
		<tr>

			<th>Season name</th>
			<th>Season starts</th>
			<th>Season ends</th>
			<th>Published?</th>
			<th>Price</th>
			<th>Action</th>
		</tr>
		<?php foreach ($periods as $period){ // iterating all queried periods and building rows
			?>
			<tr>

				<td style="text-align:center;" class="iwt_title"><span><?php echo $period->title;?></span></td>
				<td style="text-align:center;" class="iwt_start"><span><?php echo $period->startdate;?></span></td>
				<td style="text-align:center;" class="iwt_finish"><span><?php echo $period->enddate;?></span></td>
				<td style="text-align:center;" class="iwt_visible"><span><?php if($period->visible==1){?>Да<?php } else {?>Нет<?php } ?></span></td>
				<td style="text-align:center;"><?php echo $period->price;?></td>
				<td style="text-align:center;">
					<input type="checkbox" name="iwtre-del[<?php echo $period->startdate;?>]" value="<?php echo $period->startdate;?>||<?php echo $period->enddate;?>"> Delete 
					&nbsp;
				</td>
			</tr>
	<?php } // "Add new period" row is below 
	?>
	<tr>
		
		<td><input type="text" name="iwtre[new][title]" class="" id="iwt_s_title"/></td>
		<td><input type="text" name="iwtre[new][date]" class="number datepicker" id="iwt_s_date"/></td>
		<td><input type="text" name="iwtre[new][enddate]"  class="number datepicker"  id="iwt_e_date"/></td>
		<td style="text-align:center;"><input type="checkbox" name="iwtre[new][visible]" value="1"/></td>
		<td><input type="text" name="iwtre[new][price]"  class="number"/></td>
		<td><input type="submit" value="Save" class="button"/></td>
	</tr>
</table>
<script type="text/javascript">
<?php
// datepicker interface and styles
?>
jQuery( ".datepicker" ).datepicker({
	changeMonth: true,
	changeYear: true,
	dateFormat: 'yy-mm-dd'
});
jQuery('.iwt_start span').click(function(){
	jQuery('#iwt_s_date').val(jQuery(this).text());
})
jQuery('.iwt_finish span').click(function(){
	jQuery('#iwt_e_date').val(jQuery(this).text());
})
jQuery('.iwt_date_edit').click(function(){
	return false;
})
</script>
<style>
.iwt_start span, .iwt_finish span{
	border-bottom: 1px dashed green;
	text-decoration: none;
	cursor: pointer;
}
#ui-datepicker-div {
	background-color: #f0f0f0;
	padding: 10px;
	border: 1px solid #ccc;
}
</style>
<?php 
}

function iwt_re_save($post_id){
// save data on user form submit
	global $wpdb;	
	//  wp nonce checking
	if ( !wp_verify_nonce( $_POST['iwt_re_plugin'], plugin_basename(__FILE__) ) ){  // not ok
		return $post_id;
	} else {  // nonce is ok
		if(isset($_POST['iwtre-del'])){ // user wants to delete row or rows
			$del = $_POST['iwtre-del'];
			foreach ($del as $v){ // each row to separate value
				$deldates=explode('||', $v); // exploding start and end dates
				$q='DELETE FROM '.$wpdb->prefix.'iwt_re_periods WHERE date>="'.$deldates[0].'" AND date<="'.$deldates[1].'" AND postid='.$post_id;
				$wpdb->query($q);
			}
		}
		$iwtre=($_POST['iwtre']); // users fills in new row or modifing existing one
		foreach ($iwtre as $value){ // iterating all the periods
			if ($value['date']==""||$value['enddate']==""||$value['price']==""){ // checking that dates are not empty				
			}
			else {
				if (strtotime($value['date'])<strtotime($value['enddate'])){ // checking that first date really precedes the second one
					$range=iwt_createDateRangeArray($value['date'], $value['enddate']); // if so, let's create the range
					foreach ($range as $day){ //iterating the days
						// inserting new days or updating existing ones
						$q='INSERT INTO '.$wpdb->prefix.'iwt_re_periods (date, price, postid, visible, title) VALUES ("'.$day.'","'.$value['price'].'", '.$post_id.','.(int)$value['visible'].',"'.$value['title'].'" )
						ON DUPLICATE KEY UPDATE price="'.$value['price'].'"';
						$wpdb->query($q);
					}
				}
			}
		}
	}	
}
function unique_identifyer_admin_notices() { // admin notices 
	settings_errors( 'unique_identifyer' );
}
add_action( 'admin_notices', 'unique_identifyer_admin_notices' );
function iwt_re_menu(){ // creating submenu for wp-admin. Currently has no settings, but they might appear soon.
	add_submenu_page('options-general.php', 'IWT date range settings', 'IWT date range settings','manage_options', __FILE__.'iwt_re_settings_page','iwt_re_settings_page');
}
function iwt_re_install(){ 
// This function creates table for storing data. Plugin 
// stores each date in separate cell to prevent collisions.
// postid + date is unique.
	global $wpdb;
	$q = 'CREATE TABLE IF NOT EXISTS `'.$wpdb->prefix.'iwt_re_periods` (
		`date` date NOT NULL,
		`postid` bigint NOT NULL,
		`title` varchar(255) NOT NULL,
		`price` double NOT NULL,
		`status` double NOT NULL,
		`visible` tinyint NOT NULL,
		UNIQUE `postid_date` (`postid`, `date`)
		) COMMENT=\'\';';
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

dbDelta($q);
}
function iwt_re_settings_page(){
	// settings page in wp-admin
	?>
	<div class="wrap">
		This plugin has no settings. Enjoy :-)
	</div>
	<?php }
	function iwt_createDateRangeArray($strDateFrom,$strDateTo)
	{
    // takes two dates formatted as YYYY-MM-DD and creates an
    // inclusive array of the dates between the from and to dates.

    // could test validity of dates here but I'm already doing
    // that in the main script

		$aryRange=array();

		$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

		if ($iDateTo>=$iDateFrom)
		{
        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
        while ($iDateFrom<$iDateTo)
        {
            $iDateFrom+=86400; // add 24 hours
            array_push($aryRange,date('Y-m-d',$iDateFrom));
        }
    }
    return $aryRange;
}
?>